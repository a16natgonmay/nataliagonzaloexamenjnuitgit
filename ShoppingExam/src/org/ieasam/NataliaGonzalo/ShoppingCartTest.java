package testExamenNAtalia;

import static org.junit.Assert.*;

import org.iesam.GonNat.Product;
import org.iesam.GonNat.ProductNotFoundException;
import org.iesam.GonNat.ShoppingCartNataliaGonzalo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;



public class ShoppingCartTest1 {

    private ShoppingCartNataliaGonzalo _bookCart;
    private Product _defaultBook;

    @Before
    protected void prepararTest() {

        _bookCart = new ShoppingCartNataliaGonzalo();

        _defaultBook = new Product("Extreme Programming", 23.95);
        _bookCart.addItem(_defaultBook);
    }
    
    @Test
    public void compruebaVacío(){
    	_bookCart.empty();
    	boolean estaBuit = _bookCart.isEmpty();
    	assertTrue(estaBuit);
    } 

    @Test
    public void añadirProducto(){
    	Product pInicial = new Product("llibre", 2);
    	Product p3 = new Product("llibre", 2);
    	_bookCart.addItem(pInicial);
    	_bookCart.addItem(p3);
    	double suma = pInicial.getPrice() + p3.getPrice();
    	assertEquals(suma, _bookCart.getBalance());
    	assertEquals(2, _bookCart.getItemCount());
    	
    }

    @Test (expected = ProductNotFoundException.class)
    public void productoNoExistente () throws ProductNotFoundException {
    	Product book = new Product("book", 2);
  
			_bookCart.removeItem(book);
    }
    /*
    @Test 
    public void quitarProducto(){
    _bookCart.removeItem(_);
    
    }*/

	
    
    
	@After
    protected void limpiarTest() {
        _bookCart = null;
    }
}